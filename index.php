<?php
    /*
    Plugin Name: WKW Helper
    Plugin URI: https://gitlab.com/protechig/wkw-helper
    description: >-
    A WooCommerce plugin to remove functionality and customise WKW Associates' website.
    Version: 1.0
    Author: ProTech Internet Group
    Author URI: https://protechig.com/
    License: GPL2
    */

    // REMOVE TABS
    add_filter( 'woocommerce_product_tabs', 'remove_woocommerce_product_tabs', 98 );
    function remove_woocommerce_product_tabs( $tabs ) {
        unset( $tabs['description'] );
        unset( $tabs['reviews'] );
        unset( $tabs['additional_information'] );
        return $tabs;
    }

    // REMOVE RELATED PRODUCTS
    remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
    add_filter('woocommerce_product_related_posts_query', '__return_empty_array', 100);

    // REMOVE UPSELL PRODUCTS
    function remove_woocommerce_related_products() {  
        remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
        remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
        remove_action( 'woocommerce_after_single_product_summary', 'storefront_upsell_display', 15 );
    }
    add_action('init', 'remove_woocommerce_related_products', 10);

    // REMOVE CATEGORY (CURRENTLY NOT WORKING)
    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

    // REMOVE SIDEBAR ON SINGLE PRODUCT PAGES
    add_action( 'init', 'wkw_remove_sidebar' );
    function wkw_remove_sidebar() {
        if ( is_product() ) {
        remove_action( 'storefront_sidebar', 'storefront_get_sidebar', 10 );
        }
    }

    // ADD DESCRIPTION TO SUMMARY
    function woocommerce_template_product_description() {
        wc_get_template( 'single-product/tabs/description.php' );
    }
    add_action( 'woocommerce_single_product_summary', 'woocommerce_template_product_description', 20 );

    // ADD CUSTOM STYLESHEET
    wp_enqueue_style( 'style1', plugins_url( 'style.css' , __FILE__ ) );

    // ADD CUSTOM TAXONOMY "Manufacturer"
    add_action( 'init', 'custom_taxonomy_Manufacturer' );
    function custom_taxonomy_Manufacturer()  {
    $labels = array(
        'name'                       => 'Manufacturers',
        'singular_name'              => 'Manufacturer',
        'menu_name'                  => 'Manufacturer',
        'all_items'                  => 'All Manufacturers',
        'parent_item'                => 'Parent Manufacturer',
        'parent_item_colon'          => 'Parent Manufacturer:',
        'new_item_name'              => 'New Manufacturer Name',
        'add_new_item'               => 'Add New Manufacturer',
        'edit_item'                  => 'Edit Manufacturer',
        'update_item'                => 'Update Manufacturer',
        'separate_items_with_commas' => 'Separate Manufacturer with commas',
        'search_items'               => 'Search Manufacturer',
        'add_or_remove_items'        => 'Add or remove Manufacturer',
        'choose_from_most_used'      => 'Choose from the most used Manufacturers',
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'manufacturer', 'product', $args );
    register_taxonomy_for_object_type( 'manufacturer', 'product' );
    }

    // ADD CUSTOM SIDEBAR WIDGET
    // Creating the widget 
    class manufacturers_widget extends WP_Widget {
        function __construct() {
            parent::__construct(
            
            // Base ID of your widget
            'manufacturers_widget', 
            
            // Widget name will appear in UI
            __('Manufacturers Widget', 'manufacturers_widget_domain'), 
            
            // Widget description
            array( 'description' => __( 'A widget to display a list of manufacturers', 'manufacturers_widget_domain' ), ) 
            );
        }
      
        // Creating widget front-end
        public function widget( $args, $instance ) {
            $title = apply_filters( 'widget_title', $instance['title'] );
            
            // before and after widget arguments are defined by themes
            echo $args['before_widget'];
            if ( ! empty( $title ) )
            echo $args['before_title'] . $title . $args['after_title'];
            
            // This is where you run the code and display the output
            echo __( 'Hello, World!', 'manufacturers_widget_domain' );
            echo $args['after_widget'];
        }
                
        // Widget Backend 
        public function form( $instance ) {
            if ( isset( $instance[ 'title' ] ) ) {
                $title = $instance[ 'title' ];
            }
            else {
                $title = __( 'New title', 'manufacturers_widget_domain' );
            }

            // Widget admin form
            ?>
                <p>
                    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
                    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
                </p>
            <?php 
        }
          
        // Updating widget replacing old instances with new
        public function update( $new_instance, $old_instance ) {
            $instance = array();
            $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
            return $instance;
        }

    // Class wpb_widget ends here
    }
     
    // Register and load the widget
    function manufacturers_load_widget() {
        register_widget( 'manufacturers_widget' );
    }
    add_action( 'widgets_init', 'manufacturers_load_widget' );
?>
